﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebAppMVC.Infrastructure
{
    public class AppConfig
    {
        public static string CategoriesIconsFolderRelative { get; } = ConfigurationManager.AppSettings["CategoriesIconsFolder"];
        public static string PhotosFolderRelative { get; } = ConfigurationManager.AppSettings["PhotosFolder"];
    }
}