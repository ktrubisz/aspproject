﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAppMVC.Infrastructure
{
    public static class UrlHelpers
    {
        public static string CategoriesIconPath(this UrlHelper helper, string genreIconFilename)
        {
            var categoriesIconsFolder = AppConfig.CategoriesIconsFolderRelative;
            var path = Path.Combine(categoriesIconsFolder, genreIconFilename);
            var absolutePath = helper.Content(path);
            return absolutePath;
        }

        public static string PhotosPath(this UrlHelper helper, string photoFilename)
        {
            var photosFolder = AppConfig.PhotosFolderRelative;
            var path = Path.Combine(photosFolder, photoFilename);
            var absolutePath = helper.Content(path);
            return absolutePath;
        }
    }
}