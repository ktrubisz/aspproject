﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppMVC.Models;

namespace WebAppMVC.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<Category> Categories { get; set; } 
        public IEnumerable<Product> BestProducts { get; set; } 
        public IEnumerable<Product> NewProducts { get; set; }
    }
}