﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebAppMVC.Models;

namespace WebAppMVC.EntityDB
{
    public class StoreContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Order> Orders { get; set; }

        public StoreContext() : base("StoreContext")
        {
            
        }

        static StoreContext()
        {
            Database.SetInitializer<StoreContext>(new StoreInitialiazer());
        }
    }
}