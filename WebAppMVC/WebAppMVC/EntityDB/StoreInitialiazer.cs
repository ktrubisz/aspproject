﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using WebAppMVC.Models;

namespace WebAppMVC.EntityDB
{
    //public class StoreInitialiazer : DropCreateDatabaseIfModelChanges<StoreContext>
    //{

    //}

    public class StoreInitialiazer : DropCreateDatabaseAlways<StoreContext>
    {
        protected override void Seed(StoreContext context)
        {
            SeedStoreData(context);
            context.SaveChanges();
            base.Seed(context);
        }

        private void SeedStoreData(StoreContext context)
        {
            var products = new List<Product>
            {
                new Product {Name = "bed1", CategoryId = 1, Description = "123", DateAdded = DateTime.Now, IsBestPrice = true, ImageFileName = "1.png"},
                new Product {Name = "bed2", CategoryId = 1, Description = "123", DateAdded = DateTime.Now,  ImageFileName = "2.png"},
                new Product {Name = "bed3", CategoryId = 1, Description = "123", DateAdded = DateTime.Now,  ImageFileName = "3.png"},
                new Product {Name = "Sets1", CategoryId = 2, Description = "dsada", DateAdded = DateTime.Now,  ImageFileName = "4.png"},
                new Product {Name = "Sofas1", CategoryId = 3, Description = "dsada", DateAdded = DateTime.Now, IsBestPrice = true, ImageFileName = "5.png"},
                new Product {Name = "Lamps1", CategoryId = 4, Description = "dsada", DateAdded = DateTime.Now, IsBestPrice = true, ImageFileName = "6.png"},
                new Product {Name = "Lamps2", CategoryId = 4, Description = "dsada", DateAdded = DateTime.Now, ImageFileName = "7.png" },
                new Product {Name = "Lamps3", CategoryId = 4, Description = "dsada", DateAdded = DateTime.Now, ImageFileName = "8.png" },
                new Product {Name = "Carpets1", CategoryId = 5, Description = "dsada", DateAdded = DateTime.Now, IsBestPrice = true,  ImageFileName = "8.png"},
                new Product {Name = "Carpets2", CategoryId = 5, Description = "dsada", DateAdded = DateTime.Now,  ImageFileName = "9.png"},
                new Product {Name = "Chairs1", CategoryId = 6, Description = "dsada", DateAdded = DateTime.Now, IsBestPrice = true,  ImageFileName = "1.png"},
                new Product {Name = "Chairs2", CategoryId = 6, Description = "dsada", DateAdded = DateTime.Now,  ImageFileName = "2.png"}
            };
            products.ForEach(c => context.Products.AddOrUpdate(c));

            var categories = new List<Category>
            {
                new Category {Name = "Beds", IconFilename = "Beds.png", Description = "Beds desc"},
                new Category {Name = "Sets", IconFilename = "Sets.png", Description = "Sets desc"},
                new Category {Name = "Sofas", IconFilename = "Sofas.png", Description = "Sofas desc"},
                new Category {Name = "Lamps", IconFilename = "Lamps.png", Description = "Lamps desc"},
                new Category {Name = "Carpets", IconFilename = "Carpets.png", Description = "Carpets desc"},
                new Category {Name = "Chairs", IconFilename = "Chairs.png", Description = "Chairs desc"},
            };
            categories.ForEach(c => context.Categories.AddOrUpdate(c));
            //foreach (var category in categories)
            //{
            //    context.Categories.Add(category);
            //}
        }
    }
}