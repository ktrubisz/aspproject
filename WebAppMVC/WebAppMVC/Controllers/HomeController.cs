﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppMVC.EntityDB;
using WebAppMVC.Models;
using WebAppMVC.ViewModels;

namespace WebAppMVC.Controllers
{
    public class HomeController : Controller
    {
        private StoreContext _db = new StoreContext();
        public ActionResult Index()
        {
            var categories = _db.Categories;
            var newProducts = _db.Products.Where(a => !a.IsHidden).OrderBy(a => a.DateAdded).Take(3).ToList();
            var bestProducts = _db.Products.Where(a => !a.IsHidden && a.IsBestPrice).OrderBy(g => Guid.NewGuid()).Take(3). ToList();
            var homeViewModel = new HomeViewModel
            {
                Categories = categories,
                NewProducts = newProducts,
                BestProducts = bestProducts
            };
            return View(homeViewModel);
        }
        public ActionResult StaticContent(string viewname)
        {
            return View(viewname);
        }
    }
}